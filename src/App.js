import React from "react";
import "./App.css";

import HelloWorldClassComponent from "./components/HelloWorldClassComponent";
import HelloWorldPureComponent from "./components/HelloWorldPureComponent";
import HelloWorldCreateElement from "./components/HelloWorldCreateElement";
import HelloWorldFunctionalComponent from "./components/HelloWorldFunctionalComponent";

function App() {
  return (
    <div className="App">
      <main>
        {HelloWorldCreateElement}
        <HelloWorldClassComponent className="customComponent" />
        <HelloWorldPureComponent
          className="customComponent"
          text="additional text"
        />
        <HelloWorldFunctionalComponent className="customComponent" />
      </main>
    </div>
  );
}

export default App;
