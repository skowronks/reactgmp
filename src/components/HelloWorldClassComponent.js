import React, { Component } from "react";

export default class HelloWorldClassComponent extends Component {
  constructor() {
    super();
  }
  render() {
    return <h1>Hello World - Class component</h1>;
  }
}
