import React from "react";

const HelloWorldFunctionalComponent = () => (
  <h1>Hello world - Functional component</h1>
);
export default HelloWorldFunctionalComponent;
