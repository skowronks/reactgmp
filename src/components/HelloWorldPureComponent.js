import React, { PureComponent } from "react";

export default class HelloWorldPureComponent extends PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
    const { text } = this.props;
    return <h1>Hello World - Pure Component: {text}</h1>;
  }
}
